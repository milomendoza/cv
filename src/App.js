//REACT
import React, { Component } from 'react';

//CSS
import './App.css';

//PROVIDER
import {MProvider} from './provider/Provider'

//COMPONENT
import Home from './Home/Home'

class App extends Component {
  
  closeAll = () => {
    document.getElementById('mask').style.display = "none"
    document.getElementById('modal').style.display = "none"
  }

  render() {
    return (
      <div className="App-box">
          <div id="mask" className="Mask" onClick={this.closeAll}>
          </div>
         <MProvider>
          <React.Fragment>
            <Home>
            </Home>
          </React.Fragment>
        </MProvider>
        {/*<MyContext.Consumer>
          {(context) => (
            <React.Fragment>
              <p>asdadad:{context.state.languages[0].name}</p>
            </React.Fragment>
          ) }
        </MyContext.Consumer>*/}
      </div>
      
    );
  }
}

export default App;
