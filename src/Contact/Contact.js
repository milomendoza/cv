//REACT
import React from 'react';

const contanct = (props) => {
    return (
        <div> 
            { props.type === 2 &&
                <div className="Contact-item">
                    <img src={props.photo} className="icon" alt="" />
                    <a className="Contact-item-text" target="_blank" href={"https://api.whatsapp.com/send?phone=57"+props.text+"&text=Holaa%20Milo!"}> {props.text}</a>
                </div>
            }
            { props.type === 3 &&
                <div className="Contact-item">
                    <img src={props.photo} className="icon" alt="" />
                    <a className="Contact-item-text" target="_blank" href={"mailto:"+props.text}> {props.text}</a>
                </div>
            }
            { props.type === 4 &&
                <div className="Contact-item">
                    <img src={props.photo} className="icon" alt="" />
                    <a className="Contact-item-text" target="_blank" href={props.text}> {props.text}</a>
                </div>
            }
        </div> 
    )
};

export default contanct;