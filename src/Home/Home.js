//REACT
import React from 'react';
import Favicon from 'react-favicon';

//CONTEXT
import {MContext} from '../provider/Provider'

//COMPONENT
import Information from '../Information/Information'
import Left from '../Left/Left'
import Modal from '../Modal/Modal'

//IMAGES
import yo from '../imgs/yo.jpeg'

const Home = (props) => {
    
    const _right = (
        <div className="Right-content">
            <MContext.Consumer>
                {(context) => (
                    <React.Fragment>
                        {context.state.right.map((element, i) => { 
                            return <Information 
                                info={element}  
                                currentYear={context.state.currentYear}
                                key={i}
                            /> 
                        })}
                    </React.Fragment>
                )}
          </MContext.Consumer>
        </div>
      )
    
    const _left = (
        <div >
            <MContext.Consumer>
                {(context) => (
                    <React.Fragment>
                         <Left 
                            contact={context.state.contact} 
                            languages={context.state.languages}  
                            skills={context.state.skills} 
                            hoobies={context.state.hoobies} 
                            references={context.state.references}
                        /> 
                    </React.Fragment>
                )}
            </MContext.Consumer>
        </div>
    )

    const _modal = (
        <div >
            <MContext.Consumer>
                {(context) => (
                    <React.Fragment>
                         <Modal 
                            year={context.state.currentYear} 
                        /> 
                    </React.Fragment>
                )}
            </MContext.Consumer>
        </div>
    )

    return (
        <div className="App">
            {_modal}
            <div>
                <Favicon url={yo}/> 
            </div>
            {_left}
            <div className="Right">
                <label className="line">
                    <span className="Point-last"/>
                </label>
                {_right}
            </div>
        </div>
    )
}

export default Home;