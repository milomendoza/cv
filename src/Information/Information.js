//REACT
import React from 'react'

//PROVIDER
import {MContext} from '../provider/Provider'

const Information = (props) => {
 
    const _year = (
        <div className="Information-year">
            {props.info.years.map((year,i) => { 
                let _color = {color:year.color}
                return ( 
                    <div className="Information-year-wrap" key={i}>
                        <div className="Information-year-left"> 
                            <MContext.Consumer>
                                {(context) => (
                                    <React.Fragment>
                                        <span className="Point-year" id={year.id} onClick={() => context.editCurrenYear(year)}/>
                                    </React.Fragment>
                                )}
                            </MContext.Consumer>
                            <p className="Number-year" style={_color}>{year.number}</p>
                        </div>
                        <div className="Information-year-right"> 
                            {year.info.map((_info,i) => { 
                                return ( 
                                    <div key={i}>
                                        { _info.type === 1 &&
                                            <div className="Information-year-right-wrap"> 
                                                <p className="Information-content-text-title">{_info.info_title}</p>
                                                <p className="Information-content-text-subtitle">{_info.info_subtitle}</p>
                                                <p className="Information-content-text-resume">{_info.info_text}</p>
                                            </div>
                                        }
                                        { _info.type === 2 &&
                                            <div className="Information-year-right-wrap"> 
                                                <div className="Information-content-text-title">{_info.info_title}
                                                    <div className="Information-content-text-resume2">{_info.info_subtitle}
                                                    </div>
                                                </div>
                                                <p className="Information-content-text-resume">{_info.info_text}</p>
                                            </div>
                                        }
                                        { _info.type === 3 &&
                                            <div className="Information-year-right-wrap"> 
                                                <div className="Information-content-text-title">{_info.info_title === '&nbsp;' ? '' : _info.info_title }
                                                    <div className="Information-content-text-resume3">{_info.info_subtitle}
                                                    </div>
                                                </div>
                                                <p className="Information-content-text-resume"></p>
                                                {_info.list_text.map((element,i) => {
                                                    return (
                                                        <div key={i}>
                                                            <div className="Information-content-text-title">{element.title}
                                                                <div className="Information-content-text-resume3">{element.text}</div>
                                                            </div>
                                                            <p className="Information-content-text-resume"></p>
                                                        </div> 
                                                    )
                                                })}
                                            </div>
                                        }
                                        
                                    </div>

                                )
                            })}
                        </div> 
                    </div>
                )
            })}
        </div>
    )
    return (
        <div className="Information-box"> 
            <div className="Information-header"> 
                <div className="wrap-header-icon"> 
                    <img src={props.info.icon} alt="" className="Information-icon"/> 
                </div> 
                <p className="Information-header-title">{props.info.title}</p>
            </div> 
            {_year}
        </div> 
    )
    
}

export default Information