//REACT
import React from 'react'

//COMPONENTS
import Contact from '../Contact/Contact'
import Bar from '../Bar/Bar'

//IMAGES
import photo from '../imgs/robo.png'

const Left = (props) => {
    
    const _contacts = (
        <div className="Left-content-information-section">
          {props.contact.map( (element, i) => { 
              return <Contact 
                type={element.type} 
                photo={element.img}  
                text={element.text} 
                key={i}
              /> 
          })}
        </div>
      )
    
    const _languages = (
        <div className="Left-content-information-section">
            {props.languages.map((element, i) => { 
                return <Bar 
                name={element.name} 
                number={element.number}
                key={i}  
                /> 
            })}
        </div>
    )
    
    const _skills = (
        <div className="Left-content-information-section">
          {props.skills.map((element, i)  => { 
              return <Bar 
                name={element.name} 
                number={element.number} 
                key={i}   
              /> 
          })}
        </div>
    )

    const _hoobies = (
        <div className="Left-content-information-section-horizontal">
          {props.hoobies.map((element, i) => { 
              return (
                <img 
                  src={element.route} 
                  className="img-photo-hoobies" 
                  alt="" 
                  key={i}   
                />
              )
          })}
        </div>
    )

    const _references = (
        <div className="Left-content-information-section">
          {props.references.map((element, i)  => { 
              return (
                <p className="Left-content-text" key={i}> {element.text} </p>
              )
          })}
        </div>
    )

    return(
        <div className="Left">
            <div className="Left-headert">
                <div className="Left-headert-photo">
                <img src={photo} className="img-photo" alt="" />
                <div className="triangle">
                </div>
                </div>
                <div className="Left-headert-profile">
                <p className="name"> Juan Camilo Mendoza Balanta</p>
                <p className="job">Ingeniero Telemático</p>
                </div>
            </div>
            <div className="Left-content">
                <div className="Left-content-info">
                <p className="Left-content-title">perfil personal</p>
                <div className="Left-content-text">
                    Ingeniero Telemático, con interés de trabajar en las áreas 
                    de <div className="Bold-text">desarrollo web</div>, <div className="Bold-text">Gestión de empresa de Base Tecnológica</div>
                    soluciones de redes y Tecnologías de información, con experiencia en  desarrollo web, 
                    gerencia de proyectos de TIC y gestión tecnológica.
                </div>
                <div className="Left-content-text">
                    Persona líder, con habilidades de <div className="Bold-text"> trabajo en equipo</div>, 
                    excelentes relaciones interpersonales y facilidad y disposición para aprender.
                </div>
                </div>
                <div className="Left-content-info">
                <p className="Left-content-title">contacto</p>
                {_contacts}
                </div>
                <div className="Left-content-info">
                <p className="Left-content-title">idiomas</p>
                {_languages}
                </div>
                <div className="Left-content-info">
                <p className="Left-content-title">habilidades</p>
                {_skills}
                </div>
                <div className="Left-content-info">
                <p className="Left-content-title">Hobbies</p>
                {_hoobies}
                </div>
                <div className="Left-content-info">
                <p className="Left-content-title">referencias</p>
                {_references}
                </div>
            </div>
        </div>
    )
}

export default Left