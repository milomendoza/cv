import React from 'react';

const Modal = (props) => {
    console.log('Esto es lo que llega : ', props)

    let filterTitle = (_title) => {
        let _re = ''
        if(_title.includes('fa')){
            _re='Formación Academica'
        }else if(_title.includes('el')){
            _re='experiencia laboral'
        }else if(_title.includes('ip')){
            _re='INVESTIGACIONES y /o PUBLICACIONES'
        }   
        return _re
    }

    const close = () => {
        document.getElementById('mask').style.display = "none"
        document.getElementById('modal').style.display = "none"
    }

    const _view = (
        <div className="Modal-wrap">
            <div className="Modal-header">
                <p className="Modal-header-year">{`${props.year.number} - ${filterTitle(props.year.id)}`}</p>
                <span className="Xletter" onClick={() => close()}> X </span>
            </div>
            {props.year.info.map((txt, i) => {
                return(
                    <div className="Modal-Content" key={i}>
                        { txt.type === 1 &&
                            <div className="Information-year-right-wrap"> 
                                <p className="Information-content-text-title">{txt.info_title}</p>
                                <p className="Information-content-text-subtitle">{txt.info_subtitle}</p>
                                <p className="Information-content-text-resume">{txt.info_text}</p>
                            </div>
                        }
                        { txt.type === 2 &&
                            <div className="Information-year-right-wrap"> 
                                <div className="Information-content-text-title">{txt.info_title}
                                    <div className="Information-content-text-resume2">{txt.info_subtitle}
                                    </div>
                                </div>
                                <p className="Information-content-text-resume">{txt.info_text}</p>
                            </div>
                        }
                        { txt.type === 3 &&
                            <div className="Information-year-right-wrap"> 
                                <div className="Information-content-text-title">{txt.info_title === '&nbsp;' ? '' : txt.info_title }
                                    <div className="Information-content-text-resume3">{txt.info_subtitle}
                                    </div>
                                </div>
                                <p className="Information-content-text-resume"></p>
                                {txt.list_text.map((element,i) => {
                                    return (
                                        <div key={i}>
                                            <div className="Information-content-text-title">{element.title}
                                                <div className="Information-content-text-resume3">{element.text}</div>
                                            </div>
                                            <p className="Information-content-text-resume"></p>
                                        </div> 
                                    )
                                })}
                            </div>
                        }
                    </div>
                )
            })} 
        </div>
    )
    return (
        <div id="modal" className="Modal-box"> 
            {_view}
        </div> 
    )
}

export default Modal;