import React, { Component } from 'react';
import phone from './../imgs/smartphone-call.svg'
import mail from './../imgs/mail.svg'
import ln from './../imgs/linked-in-logo.svg'
import soccer from './../imgs/football.svg'
import dance from './../imgs/dancer-with-music.svg'
import fastfood from './../imgs/fast-food.svg'
import academic from './../imgs/black-graduation-cap-tool-of-university-student-for-head.svg'
import briefcase from './../imgs/briefcase.svg'
import idea from './../imgs/light-bulb.svg'

    const MyContext = React.createContext()

class Provider extends Component{
   
    state = {
        contact: [
            {type:2, img:phone, text:'3128544398'},
            {type:3, img:mail, text:'juancamilomendozabalanta.jcmb@gmail.com'},
            {type:4, img:ln, text:'https://co.linkedin.com/in/juancamilomendozabalanta'},
        ],
        languages: [
            {name:'Español', number:100},
            {name:'Ingles', number:70},
        ],
        skills: [
            {name:'Java', number:50},
            {name:'JS', number:75},
            {name:'HTML', number:75},
            {name:'CSS', number:75},
            {name:'Angular JS', number:70},
            {name:'React JS', number:60},
        ],
        hoobies: [
            {route:soccer},
            {route:dance},
            {route:fastfood},
        ],
        references: [
            {text:'Alexander Larrahondo, Ingeniero de IP y Packet Core, Movistar, Tel: 316-2100221'},
            {text:'William Nauffal, Business analysts semi senior advance, Globant, Tel: 317-3032780'},
        ],
        right:[
            { title:'formación academica',
                icon:academic,
                years:[
                        { 
                            number:'2018',
                            color:'#000000',
                            id:'fa-1',
                            info:[
                                {
                                    type:2,
                                    info_title:'Diplomado : Entorno de aplicaciones WEB',
                                    info_subtitle:' - FORMATIC ',
                                    info_text:'Analizamos problemas de ingeniería y diseñamos soluciones empresariales a los mismos mediante la adecuada selección de herramientas, lineamientos, técnicas y tecnologías web.'
                                },

                            ]
                },
                { number:'2017',
                    color:'#000000',
                    id:'fa-2',
                    info:[
                    {
                        type:1,
                        info_title:'universidad icesi',
                        info_subtitle:'Ingeniero Telemático',
                        info_text:''
                    },
                    {
                        type:1,
                        info_title:'',
                        info_subtitle:'',
                        info_text:'Curso Avanzado, Trabajo Seguro en Alturas - SENA - Junio del 2017.'
                    }
                    ]
                },
                {   number:'2016',
                    color:'#000000',
                    id:'fa-3',
                    info:[
                    {
                        type:2,
                        info_title:'ENTRENAMIENTO ESPECIALIZADO ESTRATEGIA EMPRESARIAL',
                        info_subtitle:'- MINTIC y Fedesoft - Noviembre del 2016.',
                        info_text:''
                    },
                    {
                        type:2,
                        info_title:'PROYECTO DE GRADO',
                        info_subtitle:' "Sistema de seguridad con clasificación de placas para personas con vehículos, usando visión por computadora y geoposicionamiento" .',
                        info_text:''
                    }
                    ]
                },
                ]
            },
            { title:'experiencia laboral',
                icon:briefcase,
                years:[
                { number:'2017 Actual',
                    color:'#ff9900',
                    id:'el-1',
                    info:[
                    {
                        type:3,
                        info_title:'CLIENT MAKER',
                        info_subtitle:'Desarrollador Web Frontend',
                        list_text:[
                        {
                            title:'Función principal ',
                            text:'Desarrollo web frontend de nuevas aplicaciones web y de funcionalidades adicionales a las ya existentes.'
                        },
                        {
                            title:'Logros',
                            text:'Desarrollo responsive, Angular'
                        },
                        {
                            title:'Referencias',
                            text:'Andrés Ramírez.'
                        },
                        {
                            title:'Teléfono',
                            text:'3008532906'
                        },
        
                        ]
                    },
                    ]
                },
                { number:'2016',
                    color:'#000000',
                    id:'el-2',
                    info:[
                    {
                        type:3,
                        info_title:'PRODIPAC S.A.S',
                        info_subtitle:'Servicio por consultoría.',
                        list_text:[
                        {
                            title:'Función principal ',
                            text:'Administrar el servicio de hosting mail de la compañía y consultoría para el cableado estructurado del Centro Médico en Llano Verde.'
                        },
                        {
                            title:'Logros',
                            text:'Desarrollé habilidades como el manejo de CPANEL (Framework para la gestión del hosting mail).'
                        },
                        {
                            title:'Referencias',
                            text:'Gustavo Adolfo Garcés'
                        },
                        {
                            title:'Teléfono',
                            text:'3219957356'
                        },
        
                        ]
                    },
                    ]
                },
                { number:'2016',
                    color:'#000000',
                    id:'el-3',
                    info:[
                    {
                        type:3,
                        info_title:'MARES GROUP S.A.S',
                        info_subtitle:'Ingeniero de Soporte.',
                        list_text:[
                        {
                            title:'Función principal ',
                            text:'Estudiar la factibilidad técnica de los proyectos públicos, proponer mejoras a los diseños ya existentes y realizar la propuesta económica para dichos proyectos, prestar soporte en los servicios que involucran tecnologías de la información dentro y fuera de la organización, hacer parte del grupo de investigación y diseñar el proceso de gestión tecnológica de la organización.'
                        },
                        {
                            title:'Logros',
                            text:'Desarrollé habilidades como la investigación, adquirí conocimientos técnicos, financieros y jurídicos respecto a las licitaciones públicas de tecnologías de la información.'
                        },
                        {
                            title:'Referencias',
                            text:'Gustavo Muñoz'
                        },
                        {
                            title:'Teléfono',
                            text:'3176565639'
                        },
        
                        ]
                    },
                    ]
                }
                ]
            },
            { title:'INVESTIGACIONES y /o PUBLICACIONES',
                icon:idea,
                years:[
                { number:'2015',
                    color:'#000000',
                    id:'ip-1',
                    info:[
                    {
                        type:3,
                        info_title:'&nbsp;',
                        info_subtitle:'Investigación “CADA SER HUMANO ES UNA WPAN”',
                        list_text:[]
                    },
                    ]
                }
                ]
            },
        ],
        currentYear:{ 
            number:'2017',
            color:'#000000',
            id:'fa-1',
            info:[
                {
                    type:1,
                    info_title:'universidad icesi',
                    info_subtitle:'Ingeniero Telemático',
                    info_text:''
                },
                {
                    type:1,
                    info_title:'',
                    info_subtitle:'',
                    info_text:'Curso Avanzado, Trabajo Seguro en Alturas - SENA - Junio del 2017.'
                }
            ]
        },
        
    }

    editCurrentYear = (_currentYear) => {
        document.getElementById('mask').style.display = "block"
        document.getElementById('modal').style.display = "block"      
        this.setState({
            currentYear: _currentYear
        })
    }

    //this.editCurrentYear = this.editCurrentYear.bind(this)
    
    render(){
        return(
            <MyContext.Provider value={{state: this.state, editCurrenYear: this.editCurrentYear}} >
              {this.props.children}
            </MyContext.Provider>
        )
    }
}

export const MProvider = Provider
export const  MContext = MyContext